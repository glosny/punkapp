import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import { Card, Button } from 'semantic-ui-react';

class BeersList extends Component {
  render() {
    if (this.props.beers.length > 0) {
      return (
        <div className='BeersList'>
          <Card.Group >
            {this.props.beers.map(beer => (
              <Card key={beer.id}>
                <Card.Content>
                  <Card.Header>{beer.name}</Card.Header>
                  <Card.Meta>
                    <p>Brewed before:: {beer.first_brewed}</p>

                  </Card.Meta>
                  <Card.Description>{beer.tagline}</Card.Description>
                </Card.Content>
                <Card.Content extra className='beer__extra'>
                  <span>ABV: {beer.abv}</span>
                  <Link to={{ pathname: `/beers/${beer.id}`, query: { id: beer.id } }}>
                    <Button
                      basic
                      color='green'
                      floated='right'
                    >
                      Show
                    </Button>
                  </Link>
                </Card.Content>
              </Card>
            ))}
          </Card.Group>
        </div >
      );
    }
    else {
      return (
        <div>
          <div><h1>{this.props.errors}</h1></div>
        </div>
      )
    }
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    beers: state.beers,
    errors: state.errors,
    loaded: state.loaded
  };
};

export const BeersListData = connect(
  mapStateToProps
)(BeersList);