import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { BeersListDataComponent } from '../BeersListComponent';
import { BeerDataComponent } from '../BeerViewComponent';

const Routes = () => (
  <Switch>
    <Route exact path='/beers/beerAbv=:beerAbv&beerBrew=:beerBrew' component={BeersListDataComponent} />
    <Route path='/beers/:beerId' component={BeerDataComponent} />
  </Switch>
)


export default Routes;