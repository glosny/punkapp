import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import { Grid, Image, Label, List, Button, Header, Loader, Dimmer } from 'semantic-ui-react';

class BeerView extends Component {

  render() {
    if (this.props.loaded) {
      return (
        <Dimmer active inverted>
          <Loader indeterminate>PREPARING BEER</Loader>
        </Dimmer>
      )
    }
    else {
      return (
        <div className='BeerView' >
          <Header dividing>
            <Link to={{ pathname: `/beers/beerAbv=${this.props.beerAbv}&beerBrew=${this.props.beerBrew}`}}>
              <Button primary fluid>
                BACK TO BEERS
          </Button>
            </Link>
          </Header>
          {this.props.beerData.map(beer => (
            <Grid key={beer.id}>
              <Grid.Column width={6}>
                <Image
                  height='200px'
                  src={beer.image_url}
                  label={{ color: 'black', content: `FIRST BREWED AT: ${beer.first_brewed} `, ribbon: true }}
                />
              </Grid.Column>
              <Grid.Column width={10}>
                <h1>{beer.name}</h1>
                <p>{beer.description}</p>
                <Label.Group size='huge'>
                  <Label>ABV: {beer.abv}</Label>
                  <Label>IBU: {beer.ibu}</Label>
                  <Label>EBC: {beer.ebc}</Label>
                </Label.Group>
                <h4>Fits well with this kind of food:</h4>
                <List divided relaxed>
                  <List.Item>{beer.food_pairing[0]}</List.Item>
                  <List.Item>{beer.food_pairing[1]}</List.Item>
                  <List.Item>{beer.food_pairing[2]}</List.Item>
                </List>
              </Grid.Column>
            </Grid>
          ))}
        </div >
      )
    }
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    errors: state.errors,
    loaded: state.loaded,
    beerData: state.beerData,
    beerAbv: state.beerAbv,
    beerBrew: state.beerBrew
  };
};

export const BeerData = connect(
  mapStateToProps,
)(BeerView);
