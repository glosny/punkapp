import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchBeers } from '../actions/beers';
import { Link } from 'react-router-dom';
import Routes from './routes/Routes';

import 'semantic-ui-css/semantic.min.css';
import {
  Button, Form, Segment, Header, Container,
} from 'semantic-ui-react';

class App extends Component {
  constructor() {
    super();
    this.state = {
      valid: false,
      errorAbv: '',
      errorBrew: '',
      beerAbv: '',
      beerBrew: '',
    }
  }

  handleInputChange = (event, data) => {
    this.setState({
      [data.name]: event.target.value
    })
  }

  clickIt = () => {
    this.props.fetchBeers(this.state);
  }

  render() {
    return (
      <div className='App'>
        <Segment>
          <Header content='Beer with PUNKAPI' />
        </Segment>
        <Container text>
          <Form>
            <Form.Field>
              <h2>Looking for a beer?</h2>
              <Form.Group>
                <Form.Input
                  label='Beer ABV - Alcohol Volume:'
                  placeholder='e.g. 5, 6'
                  name='beerAbv'
                  onChange={this.handleInputChange}
                  value={this.beerAbv}
                  type='number'
                />
                {this.state.errorAbv}
                <Form.Input
                  label='Brewed before (date: month-year):'
                  placeholder='e.g. 11-2012'
                  name='beerBrew'
                  onChange={this.handleInputChange}
                  value={this.beerBrew}
                />
                {this.state.errorBrew}
                <Link to={{ pathname: `/beers/beerAbv=${this.state.beerAbv}&beerBrew=${this.state.beerBrew}` }}>
                  <Button
                    size='massive'
                    onClick={this.clickIt}
                  >
                    Search
                  </Button>
                </Link>
              </Form.Group>
            </Form.Field>
          </Form>
          <Routes />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    beers: state.beers,
    errors: state.errors,
    beerAbv: state.beerAbv,
    beerBrew: state.beerBrew
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchBeers: (state) => { dispatch(fetchBeers(state.beerAbv, state.beerBrew)) }
})

export default connect(mapStateToProps, mapDispatchToProps, null, {
  pure: false
})(App)
