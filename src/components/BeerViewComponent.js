import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchBeerData } from '../actions/beers';

import { BeerData } from './BeerView';

class BeerViewComponent extends Component {
  componentDidMount() {
    const thisBeerId = this.props.match.params.beerId;
    this.props.fetchBeerData(thisBeerId)
  }

  render() {
    return (
      <div>
        <BeerData />
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    errors: state.errors,
    beerData: state.beerData
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchBeerData: (beer) => { dispatch(fetchBeerData(beer)) },
})

export const BeerDataComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BeerViewComponent);


