import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BeersListData } from './BeersList';

import 'semantic-ui-css/semantic.min.css';
import { Loader, Dimmer } from 'semantic-ui-react';

class BeersList extends Component {

  render() {
    if (this.props.loaded) {
      return (
        <Dimmer active inverted>
          <Loader indeterminate>LOOKING FOR A BEERS...</Loader>
        </Dimmer>
      )
    }
    else {
      return (
        <div className='BeersList'>
          <BeersListData />
        </div >
      );
    }
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    beers: state.beers,
    errors: state.errors,
    loaded: state.loaded
  };
};

export const BeersListDataComponent = connect(
  mapStateToProps
)(BeersList);