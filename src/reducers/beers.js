import { actionTypes } from '../actions/actionTypes';

const initialState = {
  beers: [],
  beerAbv: '',
  beerBrew: '',
  errors: [],
  loaded: false,
  beerData: [],
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_BEERS_QUERY:
      return {
        ...state,
        loaded: true,        
        beerAbv: action.beerAbv,
        beerBrew: action.beerBrew
      }
    case actionTypes.BEER_DATA_QUERY:
      return {
        ...state,
        loaded: true,
        beerId: action.beerId,
      }
    case actionTypes.FETCH_BEERS_SUCCESS:
      return {
        ...state,
        loaded: false,
        beers: action.beers,
        errors: initialState.errors,
      }
    case actionTypes.FETCH_BEERS_ERROR:
      return {
        ...state,
        loaded: false,
        beers: initialState.beers,
        errors: action.errors,
      }

    case actionTypes.BEER_DATA_SUCCESS:
      return {
        ...state,
        loaded: false,
        beerData: action.beerData,
        errors: initialState.errors,
      }
    case actionTypes.BEER_DATA_ERROR:
      return {
        ...state,
        loaded: false,
        beerData: initialState.beerData,
        errors: action.errors,
      }

    default:
      return state;
  }
}
