export const actionTypes = {
  FETCH_BEERS_QUERY: 'Fetching selected beers',
  FETCH_BEERS_SUCCESS: 'Fetching selected beers success',
  FETCH_BEERS_ERROR: 'Fetching selected beers error',
  BEER_DATA_QUERY: 'Fetching selected beer',
  BEER_DATA_SUCCESS: 'Fetching selected beer success',
  BEER_DATA_ERROR: 'Fetching selected beer error',
}
