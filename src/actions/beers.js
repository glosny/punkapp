import { actionTypes } from './actionTypes';

const beersFetchQuery = (beerAbv, beerBrew) => ({
  type: actionTypes.FETCH_BEERS_QUERY,
  beerAbv, 
  beerBrew
});
const beersFetched = beers => ({
  type: actionTypes.FETCH_BEERS_SUCCESS,
  beers
});
const beersFetchError = errors => ({
  type: actionTypes.FETCH_BEERS_ERROR,
  errors
});


const beerDataQuery = beerId => ({
  type: actionTypes.BEER_DATA_QUERY,
  beerId
});
const beerDataSuccess = beerData => ({
  type: actionTypes.BEER_DATA_SUCCESS,
  beerData
});
const beerDataError = errors => ({
  type: actionTypes.BEER_DATA_ERROR,
  errors
});

export const fetchBeers = (beerAbv, beerBrew) => (dispatch) => {
  dispatch(beersFetchQuery(beerAbv, beerBrew)); 

  fetch(`https://api.punkapi.com/v2/beers?abv_gt=${beerAbv}&brewed_before=${beerBrew}`)
    .then(res => res.json())
    .then(json => {
      if (json.length > 0) {
        dispatch(beersFetched(json))
      }
      else {
        dispatch(beersFetchError("There's no beers you are looking for :("))
      }
    });
   // .catch(err => dispatch(beersFetchError(err.error))); catch nie catchowal bledow, api zawsze zwraca success, nawet jak jest pusty arr
};

//GET ONE BEER DATA
export const fetchBeerData = (beerId) => (dispatch, getState) => {
  dispatch(beerDataQuery());
  
  fetch(`https://api.punkapi.com/v2/beers/${beerId}`)
    .then(res => res.json())
    .then(json => dispatch(beerDataSuccess(json)))
    .catch(err => dispatch(beerDataError(err.error)));
};
