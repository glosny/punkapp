import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './reducers/beers';

const enhancer = composeWithDevTools(
  applyMiddleware(thunkMiddleware),
);

const store = createStore(reducer, enhancer);

export default store;